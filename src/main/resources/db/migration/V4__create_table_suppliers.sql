create table suppliers
(
    id             bigserial NOT NULL,
    name           varchar   NOT NULL,
    email          varchar,
    contact_person varchar,
    contact_phone  varchar,
    web_address    varchar,
    address        varchar,
    constraint supplier_PK primary key (id)
);