create table measures
(
    id   bigserial NOT NULL,
    type varchar   NOT NULL,
    constraint measure_PK primary key (id)
);