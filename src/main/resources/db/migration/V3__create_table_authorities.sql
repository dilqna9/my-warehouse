create table authorities
(
    username  varchar(50) NOT NULL,
    authority varchar     NOT NULL,
    constraint authority_PK primary key (username),
    constraint authority_FK foreign key (username) references users (username)
);