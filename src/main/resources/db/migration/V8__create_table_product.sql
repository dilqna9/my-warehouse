create table product
(
    id                   bigserial NOT NULL,
    type_id              int8      NOT NULL,
    trade_mark_id        int8      NOT NULL,
    model                varchar   NOT NULL,
    description          varchar   NOT NULL,
    supplier_id          int8      NOT NULL,
    measure_id           int8      NOT NULL,
    price_per_measure    decimal   NOT NULL,
    manufacturing_number varchar,
    is_deleted           boolean,
    constraint product_PK primary key (id),
    constraint supplier_FK foreign key (supplier_id) references suppliers (id),
    constraint measure_FK foreign key (measure_id) references measures (id),
    constraint type_FK foreign key (type_id) references types (id),
    constraint trade_mark_FK foreign key (trade_mark_id) references trade_marks (id)
);