create table types
(
    id   bigserial NOT NULL,
    name varchar   NOT NULL,
    constraint type_PK primary key (id)
);