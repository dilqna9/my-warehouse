create table users
(
    username        varchar(50) NOT NULL,
    password        varchar     NOT NULL,
    enabled         boolean     NOT NULL,
    user_details_id int8        NOT NULL,
    constraint user_PK primary key (username),
    constraint user_details_id_FK foreign key (user_details_id) references user_details (id)
);