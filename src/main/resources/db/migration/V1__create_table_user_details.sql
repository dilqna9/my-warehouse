CREATE TABLE user_details
(
    id         bigserial NOT NULL,
    first_name varchar   NOT NULL,
    last_name  varchar   NOT NULL,
    email      varchar   NOT NULL,
    phone      varchar,
    constraint user_detail_PK primary key (id)
);