create table trade_marks
(
    id   bigserial NOT NULL,
    name varchar   NOT NULL,
    constraint trade_mark_PK primary key (id)
);